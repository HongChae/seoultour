//	서울 여행 관련 사이트 링크

package seoultour.project.team;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

public class InformationActivity extends Activity {

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.information);
		// TODO Auto-generated method stub

		overridePendingTransition(android.R.anim.slide_in_left, 0);

		// 여행 관련 사이트를 링크
		// 텍스트 뷰 터치시 해당 웹페이지로 이동됨

		TextView link1 = (TextView) findViewById(R.id.link1);
		TextView link2 = (TextView) findViewById(R.id.link2);
		TextView link3 = (TextView) findViewById(R.id.link3);
		TextView link4 = (TextView) findViewById(R.id.link4);
		TextView link5 = (TextView) findViewById(R.id.link5);

		link1.setText(Html.fromHtml("<a href = "
				+ "http://www.visitseoul.net/kr/index.do?_method=main" + ">"
				+ "서울시 공식 문화관광 포털" + "</a>"));
		link1.setBackgroundColor(Color.WHITE);
		link1.setMovementMethod(LinkMovementMethod.getInstance());

		link2.setText(Html.fromHtml("<a href = "
				+ "http://www.hotelnjoy.com/main/main/" + ">" + "호텔 예약 사이트"
				+ "</a>"));
		link2.setBackgroundColor(Color.WHITE);
		link2.setMovementMethod(LinkMovementMethod.getInstance());

		link3.setText(Html.fromHtml("<a href = " + "http://topis.seoul.go.kr/"
				+ ">" + "서울특별시 교통정보 센터" + "</a>"));
		link3.setBackgroundColor(Color.WHITE);
		link3.setMovementMethod(LinkMovementMethod.getInstance());

		link4.setText(Html
				.fromHtml("<a href = " + "http://www.seoulcitybus.com/" + ">"
						+ "서울 시티 투어 버스" + "</a>"));
		link4.setBackgroundColor(Color.WHITE);
		link4.setMovementMethod(LinkMovementMethod.getInstance());

		link5.setText(Html.fromHtml("<a href = "
				+ "http://www.seoul.go.kr/main/index.html" + ">" + "서울 시청"
				+ "</a>"));
		link5.setBackgroundColor(Color.WHITE);
		link5.setMovementMethod(LinkMovementMethod.getInstance());
	}

	public void onBackPressed() {
		this.finish();
		overridePendingTransition(0, android.R.anim.slide_out_right);
	}

}

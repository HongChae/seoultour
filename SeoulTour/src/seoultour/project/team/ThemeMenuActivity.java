//	테마여행의 하위 메뉴액티비티
//
//	해당 테마 터치시 지도를 보여주는 액티비티가 실행됨

package seoultour.project.team;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class ThemeMenuActivity extends Activity {

	private ImageView themeButtonImage;
	private ImageView arButtonImeage;
	private ImageView infoButtonImage;
	private ImageView seeButtonImage;
	private ImageView shopButtonImage;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		setContentView(R.layout.thememenu);
		overridePendingTransition(android.R.anim.slide_in_left, 0);

		// 고궁, 문화, 전시, 전경, 쇼핑 메뉴
		// 터치시 해당하는 테마이름을 액티비티가 전환될때 같이 넘겨줌
		themeButtonImage = (ImageView) findViewById(R.id.lkung);
		themeButtonImage.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				themeButtonImage.setColorFilter(0xaa4169e1,
						android.graphics.PorterDuff.Mode.SRC_OVER);
				Intent intent = new Intent(getBaseContext(),
						SeoulMapActivity.class);
				intent.putExtra("theme", DBHandler.PALACE);
				startActivity(intent);
			}
		});

		arButtonImeage = (ImageView) findViewById(R.id.lculture);
		arButtonImeage.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				arButtonImeage.setColorFilter(0xaa4169e1,
						android.graphics.PorterDuff.Mode.SRC_OVER);
				Intent intent = new Intent(getBaseContext(),
						SeoulMapActivity.class);
				intent.putExtra("theme", DBHandler.CULTURE);
				startActivity(intent);
			}
		});

		infoButtonImage = (ImageView) findViewById(R.id.lmuseum);
		infoButtonImage.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				infoButtonImage.setColorFilter(0xaa4169e1,
						android.graphics.PorterDuff.Mode.SRC_OVER);
				Intent intent = new Intent(getBaseContext(),
						SeoulMapActivity.class);
				intent.putExtra("theme", DBHandler.MUSEUM);
				startActivity(intent);
			}
		});

		seeButtonImage = (ImageView) findViewById(R.id.lsee);
		seeButtonImage.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				seeButtonImage.setColorFilter(0xaa4169e1,
						android.graphics.PorterDuff.Mode.SRC_OVER);
				Intent intent = new Intent(getBaseContext(),
						SeoulMapActivity.class);
				intent.putExtra("theme", DBHandler.LANDSCAPE);
				startActivity(intent);
			}
		});

		shopButtonImage = (ImageView) findViewById(R.id.lshop);
		shopButtonImage.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				shopButtonImage.setColorFilter(0xaa4169e1,
						android.graphics.PorterDuff.Mode.SRC_OVER);
				Intent intent = new Intent(getBaseContext(),
						SeoulMapActivity.class);
				intent.putExtra("theme", DBHandler.SHOPPING);
				startActivity(intent);
			}
		});
	}

	public void onBackPressed() {
		this.finish();
		overridePendingTransition(0, android.R.anim.slide_out_right);
	}
}